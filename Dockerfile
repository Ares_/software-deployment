FROM debian
RUN apt-get update \
 && apt-get install -y curl
ENV NVM_DIR /root/.nvm/
RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.3/install.sh | bash
ADD . /opt/docker/software
RUN chmod +x /opt/docker/software/setup.sh \
 && /opt/docker/software/setup.sh \
 && chmod +x /opt/docker/software/deploy.sh
EXPOSE 80
ENTRYPOINT ["/opt/docker/software/deploy.sh"]