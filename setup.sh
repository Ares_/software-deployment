export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
nvm install 15.3.0
mkdir staging
mkdir pre-production
mkdir production
touch server.js
echo "
const env = process.argv.slice(2,3);
const message = process.argv.slice(3,4);
console.log(env);
console.log(message);
const http = require('http');
const server = http.createServer(function (request, response) {
    if (request.url === ('/' + env)) {
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.write('<h1>' + message + '</h1>');
        response.end();

    } else if (request.url === '/staging') {
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.write('<h1>Hello staging</h1>');
        response.end();

    } else if (request.url === '/pre-production') {
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.write('<h1>Hello pre-production</h1>');
        response.end();

    } else if (request.url === '/production') {
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.write('<h1>Hello production</h1>');
        response.end();
    } else {
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.write('<h1>Cher stagiaire, merci de demander plus d\'information à votre responsable</h1>');
        response.end();
    }
});
server.listen(80);

" > server.js

echo "SUCCESS"
echo "You can now deploy your environment"