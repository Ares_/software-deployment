#!/bin/bash
# deploy.sh

environment=$1
string=$2

export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

echo "L'environment est : $environment"
echo "Le message est : $string"

nvm run 15.3.0 server.js "$environment" "$string"
